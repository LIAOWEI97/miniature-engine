package com.liaowei.engine.connect.net;

import com.liaowei.engine.connect.config.ServerConfig;
import com.liaowei.engine.connect.executor.HttpProcessor;
import com.liaowei.engine.http.executor.ExecutorServiceBuilder;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 网络连接
 *
 * @author liaowei
 * @date 2021/06/11
 */
public class Connector {

    public static void run(ServerConfig config) {
        ServerSocket serverSocket = null;
        ExecutorServiceBuilder serviceBuilder = new ExecutorServiceBuilder();
        ExecutorService executorService = serviceBuilder.setCorePoolSize(2)
                .setMaximumPoolSize(10)
                .setKeepAliveTime(3, TimeUnit.MINUTES)
                .setRejectedHandler(new ThreadPoolExecutor.DiscardPolicy())
                .build();

        try {
            serverSocket = new ServerSocket();
            serverSocket.bind(new InetSocketAddress("127.0.0.1", config.getPort()));
        } catch (IOException e) {
            System.exit(1);
        }

        while (true) {
            Socket socket;
            try {
                socket = serverSocket.accept();
            } catch (IOException e) {
                continue;
            }
            executorService.execute(new HttpProcessor(socket));
        }
    }
}
