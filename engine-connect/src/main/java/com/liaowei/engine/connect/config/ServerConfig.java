package com.liaowei.engine.connect.config;

/**
 * http服务器配置类
 *
 * @author liaowei
 * @date 2021/06/11
 */
public class ServerConfig {
    private int port;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
