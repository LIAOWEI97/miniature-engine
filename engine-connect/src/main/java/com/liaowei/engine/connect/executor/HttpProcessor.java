package com.liaowei.engine.connect.executor;

import com.liaowei.engine.http.parse.HttpRequestParse;
import om.liaowei.engine.servlet.request.Request;
import om.liaowei.engine.servlet.response.Response;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 连接处理
 *
 * @author liaowei
 * @date 2021/06/11
 */
public class HttpProcessor implements Runnable {
    private Socket socket;

    public HttpProcessor(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            Map<String, Object> messageMap = reader(socket.getInputStream());
            if (messageMap == null) {
                return;
            }
            Inet4Address inetAddress = (Inet4Address) socket.getInetAddress();
            InetSocketAddress remoteSocketAddress = (InetSocketAddress) socket.getRemoteSocketAddress();

            messageMap.put("remoteHost", remoteSocketAddress.getHostString());
            messageMap.put("localhost", inetAddress.getHostAddress());
            Request request = HttpRequestParse.parse(messageMap);
            writer(socket.getOutputStream(), null);
            socket.close();
        } catch (IOException e) {

        }

    }

    private Map<String, Object> reader(InputStream inputStream) throws IOException {
        Map<String, Object> messageMap = null;
        byte[] bytes = new byte[128];
        StringBuilder sb = new StringBuilder();
        int i;
        while ((i = inputStream.read(bytes)) != -1) {
            sb.append(new String(bytes, 0, i));
            if (i < 128) {
                break;
            }
        }
        String request = sb.toString();
        if (StringUtils.isNotBlank(request)) {
            messageMap = new HashMap<>(16);
            int index = request.indexOf("\r\n");
            String requestLine = request.substring(0, index);
            messageMap.put("requestLine", requestLine);
            int index1 = request.indexOf("\r\n\r\n");
            if (index1 != -1) {
                String requestHeader = request.substring(index + 2, index1);
                messageMap.put("requestHeader", requestHeader);
                String requestBody = request.substring(index1 + 4);
                messageMap.put("requestBody", requestBody);
            } else {
                String requestHeader = request.substring(index + 2);
                messageMap.put("requestHeader", requestHeader);
            }
        }
        return messageMap;
    }

    private void writer(OutputStream outputStream, Response response) {
        PrintWriter pw = new PrintWriter(outputStream);
        pw.write("HTTP/1.1 200 OK\r\n\r\nsuccess\r\n");
        pw.flush();
    }
}
