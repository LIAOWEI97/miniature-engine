package om.liaowei.engine.servlet.request;

import java.util.Map;

/**
 * http请求接口
 *
 * @author liaowei
 * @date 2020/06/11
 */
public interface Request {
    /**
     * 获取协议信息
     * @return http/1.0 http/1.1 http/2.0
     */
    String getProtocol();

    /**
     * 获取请求方法类型
     * @return GET PUT DELETE POST HEAD
     *          CONNECT OPTIONS TRACE
     */
    String getMethod();

    /**
     * 获取远程主机地址
     * @return 公网地址
     */
    String getRemoteHost();

    /**
     * 获取远程主机地址
     * @return 私网地址
     */
    String getLocalHost();

    /**
     * 获取url中的参数
     * @param key 参数名
     * @return 参数值
     */
    Object getParameter(String key);

    /**
     * 获取请求头信息
     * @param key 请求头名称
     * @return 请求头内容
     */
    Object getHeader(String key);

    /**
     * 获取请求头信息
     * @return 请求头信息
     */
    Map<String, Object> getHeaders();

    /**
     * 获取编码类型
     * HTML文档标记：text/html;
     * 普通ASCII文档标记：text/html;
     * JPEG图片标记：image/jpeg;
     * GIF图片标记：image/gif;
     * js文档标记：application/javascript;
     * xml文件标记：application/xml;
     * @return
     */
    String getContentType();

    /**
     * 获取资源路径
     * @return
     */
    String getUrl();

    /**
     * 获取请求体
     * @return
     */
    String getRequestBody();
}
