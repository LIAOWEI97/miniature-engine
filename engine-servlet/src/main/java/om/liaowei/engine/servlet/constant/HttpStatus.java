package om.liaowei.engine.servlet.constant;

/**
 * http状态码
 *
 * @author liaowei
 * @date 2020/06/11
 */
public class HttpStatus {
    /**请求正常处理*/
    public static final String OK = "200";
    /**请求处理成功, 但没有资源返回*/
    public static final String NO_CONTENT = "204";
    /**范围请求*/
    public static final String PARTIAL_CONTENT = "206";
    /**资源URI已更新*/
    public static final String MOVED_PERMANENTLY = "301";
    /**资源的URI已临时定位到其他位置*/
    public static final String FOUND = "302";
    /**请求的资源存在另一个URI, 应使用GET定向获取请求资源*/
    public static final String SEE_OTHER = "303";
    /**资源已找到, 但未符合条件请求*/
    public static final String NOT_MODIFIED = "304";
    /**临时重定向*/
    public static final String TEMPORARY_REDIRECT = "307";
    /**请求存在语法错误*/
    public static final String BAD_REQUEST = "400";
    /**该请求需要通过http认证*/
    public static final String UNAUTHORIZED = "401";
    /**服务器拒绝访问*/
    public static final String FORBIDDEN = "403";
    /**服务器没有请求资源*/
    public static final String NOT_FOUND = "404";
    /**服务器内部错误*/
    public static final String INTERNAL_SERVER = "500";
    /**服务器暂时处于超负载或正在进行停机维护*/
    public static final String SERVICE_UNAVAILABLE = "503";
}
