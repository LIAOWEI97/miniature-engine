package om.liaowei.engine.servlet.request;

import java.util.Collections;
import java.util.Map;

/**
 * 默认请求实现
 *
 * @author liaowei
 * @date 2021/06/11
 */
public class DefaultRequest implements Request {
    /**协议 + 版本号*/
    private String protocol;
    /**请求方法*/
    private String method;
    private String remoteHost;
    private String localhost;
    private Map<String, Object> parameters;
    private Map<String, Object> headers;
    private String url;
    private String requestBody;

    @Override
    public String getProtocol() {
        return protocol;
    }

    protected void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    @Override
    public String getMethod() {
        return method;
    }

    protected void setMethod(String method) {
        this.method = method;
    }

    @Override
    public String getRemoteHost() {
        return remoteHost;
    }

    protected void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    @Override
    public String getLocalHost() {
        return localhost;
    }

    protected void setLocalhost(String localhost) {
        this.localhost = localhost;
    }

    @Override
    public Object getParameter(String key) {
        return parameters.get(key);
    }

    protected void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    @Override
    public Object getHeader(String key) {
        return headers.get(key);
    }

    protected void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    @Override
    public Map<String, Object> getHeaders() {
        return Collections.unmodifiableMap(headers);
    }

    @Override
    public String getContentType() {
        return (String) headers.get("content-type");
    }

    @Override
    public String getUrl() {
        return url;
    }

    protected void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }
}
