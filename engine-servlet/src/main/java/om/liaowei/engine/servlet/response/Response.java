package om.liaowei.engine.servlet.response;

/**
 * http响应接口
 *
 * @author liaowei
 * @date 2020/06/11
 */
public interface Response {
    /**
     * 获取协议信息
     * @return http/1.0 http/1.1 http/2.0
     */
    String getProtocol();
    /**
     * 设置协议信息
     * @param protocol http/1.0 http/1.1 http/2.0
     */
    void setProtocol(String protocol);

    /**
     * 获取状态码
     * @return
     */
    String getStatusCode();
    /**
     * 设置状态码
     * @param statusCode
     */
    void setStatusCode(String statusCode);
    /**
     * 获取响应头信息
     * @param key 请求头名称
     * @return 请求头内容
     */
    String getHeader(String key);
    /**
     * 添加响应头信息
     * @param key 响应头名称
     * @param value 响应头内容
     */
    void addHeader(String key, Object value);
    /**
     * 设置响应头信息
     * @param key 响应头名称
     * @param value 响应头内容
     */
    void setHeader(String key, Object value);
    /**
     * 获取编码类型
     * HTML文档标记：text/html;
     * 普通ASCII文档标记：text/html;
     * JPEG图片标记：image/jpeg;
     * GIF图片标记：image/gif;
     * js文档标记：application/javascript;
     * xml文件标记：application/xml;
     * @return
     */
    String getContentType();

    /**
     * 设置编码类型
     * @param contentType
     */
    void setContentType(String contentType);

    /**
     * 获取响应体
     * @return
     */
    String getResponseBody();

    /**
     * 设置响应体
     * @param responseBody
     */
    void setResponseBody(String responseBody);
}
