package om.liaowei.engine.servlet.request;

import com.liaowei.engine.commons.argument.EngineArgumentUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yss
 */
public class DefaultRequestBuilder {
    private DefaultRequest request;

    public DefaultRequestBuilder() {
        request = new DefaultRequest();
    }

    public DefaultRequestBuilder setProtocol(String protocol) {
        if (!EngineArgumentUtil.isBlank(protocol)) {
            request.setProtocol(protocol);
        }
        return this;
    }

    public DefaultRequestBuilder setMethod(String method) {
        if (!EngineArgumentUtil.isBlank(method)) {
            request.setMethod(method);
        }
        return this;
    }

    public DefaultRequestBuilder setRemoteHost(String remoteHost) {
        if (!EngineArgumentUtil.isBlank(remoteHost)) {
            request.setRemoteHost(remoteHost);
        }
        return this;
    }

    public DefaultRequestBuilder setLocalhost(String localhost) {
        if (!EngineArgumentUtil.isBlank(localhost)) {
            request.setLocalhost(localhost);
        }
        return this;
    }

    public DefaultRequestBuilder setParameters(Map<String, Object> parameters) {
        if (parameters == null) {
            request.setParameters(new HashMap<>(16));
        } else {
            request.setParameters(parameters);
        }
        return this;
    }

    public DefaultRequestBuilder setHeaders(Map<String, Object> headers) {
        if (headers == null) {
            request.setHeaders(new HashMap<>(16));
        } else {
            request.setHeaders(headers);
        }
        return this;
    }

    public DefaultRequestBuilder setUrl(String url) {
        if (!EngineArgumentUtil.isBlank(url)) {
            request.setUrl(url);
        }
        return this;
    }

    public DefaultRequestBuilder setRequestBody(String requestBody) {
        request.setRequestBody(requestBody);
        return this;
    }

    public Request build() {
        return request;
    }
}
