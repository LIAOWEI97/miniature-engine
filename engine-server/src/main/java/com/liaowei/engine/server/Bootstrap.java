package com.liaowei.engine.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.liaowei.engine.commons.loader.ClassPathResourceUtil;
import com.liaowei.engine.connect.config.ServerConfig;
import com.liaowei.engine.connect.net.Connector;

import java.io.File;
import java.io.IOException;

/**
 * @author liaowei
 * @date 2021/06/13
 */
public class Bootstrap {
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        ServerConfig serverConfig = null;
        try {
            serverConfig = bootstrap.parseYml();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Connector.run(serverConfig);
    }

    public ServerConfig parseYml() throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.findAndRegisterModules();
        String path = ClassPathResourceUtil.getResourcePath("application.yml");
        ServerConfig serverConfig = mapper.readValue(new File(path), ServerConfig.class);
        return serverConfig;
    }

    public void writeYml(ServerConfig serverConfig) throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.disable(SerializationFeature.WRITE_DATE_KEYS_AS_TIMESTAMPS);
        mapper.writeValue(new File(""), serverConfig);
    }
}
