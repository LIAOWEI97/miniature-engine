package com.liaowei.engine.commons.loader;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;

/**
 * 资源文件加载器
 *
 * @author liaowei
 * @date 2021/06/13
 */
public class ClassPathResourceUtil {
    private static final ClassLoader classLoader = ClassPathResourceUtil.class.getClassLoader();

    public static InputStream getResourceAsStream(String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
       return classLoader.getResourceAsStream(name);
    }

    public static URL getResource(String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        return classLoader.getResource(name);
    }

    public static String getResourcePath(String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        return getResource(name).getPath();
    }

    public static Enumeration<URL> getResources(String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        try {
            return classLoader.getResources(name);
        } catch (IOException e) {
            return null;
        }
    }
}
