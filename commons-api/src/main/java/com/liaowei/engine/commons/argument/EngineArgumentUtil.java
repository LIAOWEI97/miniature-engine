package com.liaowei.engine.commons.argument;

import org.apache.commons.lang3.StringUtils;

/**
 * 参数校验
 *
 * @author liaowei
 * @date 2021/06/11
 */
public class EngineArgumentUtil {
    public static boolean isEmpty(Object arg) {
        if (arg == null) {
            throw new IllegalArgumentException("arg is empty.");
        }
        return false;
    }

    public static boolean isBlank(String arg) {
        if (StringUtils.isBlank(arg)) {
            throw new IllegalArgumentException("arg is blank.");
        }
        return false;
    }
}
