package com.liaowei.engine.http.parse;

import com.liaowei.engine.commons.argument.EngineArgumentUtil;
import om.liaowei.engine.servlet.request.DefaultRequestBuilder;
import om.liaowei.engine.servlet.request.Request;

import java.util.HashMap;
import java.util.Map;

/**
 * http请求解析
 *
 * @author liaowei
 * @date 2021/06/11
 */
public class HttpRequestParse {

    public static Request parse(Map<String, Object> messageMap) {

        DefaultRequestBuilder requestBuilder = new DefaultRequestBuilder();
        String requestLine = (String) messageMap.get("requestLine");
        String requestHeader = (String) messageMap.get("requestHeader");
        String requestBody = (String) messageMap.get("requestBody");
        String remoteHost = (String) messageMap.get("remoteHost");
        String localhost = (String) messageMap.get("localhost");

        parseRequestLine(requestBuilder, requestLine);
        parseRequestHeader(requestBuilder, requestHeader);
        parseRequestBody(requestBuilder, requestBody);

        return requestBuilder.setRemoteHost(remoteHost)
                .setLocalhost(localhost)
                .build();
    }

    private static void parseRequestLine(DefaultRequestBuilder requestBuilder, String requestLine) {
        String[] lines = requestLine.split(" ");

        String method = lines[0];
        String url = lines[1];
        String protocol = lines[2];

        String[] parameters;
        if (url.indexOf("?") == -1) {
            parameters = new String[]{};
        } else {
            String params = url.substring(url.indexOf("?") + 1);
            parameters = params.split("&");
            url = url.substring(0, url.indexOf("?"));
        }
        Map<String, Object> parameterMap = new HashMap<>(16);

        for (int i = 0; i < parameters.length; i++) {
            String[] parameter = parameters[i].split("=");
            parameterMap.put(parameter[0], parameter[1]);
        }
        requestBuilder.setMethod(method)
                .setUrl(url)
                .setProtocol(protocol)
                .setParameters(parameterMap);
    }

    private static void parseRequestHeader(DefaultRequestBuilder requestBuilder, String requestHeader) {
        String[] headers = requestHeader.split("\r\n");
        Map<String, Object> headersMap = new HashMap<>(16);

        for (int i = 0; i < headers.length; i++) {
            String[] header = headers[i].split(":");
            String key = header[0];
            String value = header[1];
            headersMap.put(key, value);
        }

        requestBuilder.setHeaders(headersMap);
    }

    private static void parseRequestBody(DefaultRequestBuilder requestBuilder, String requestBody) {
        requestBuilder.setRequestBody(requestBody);
    }

}
