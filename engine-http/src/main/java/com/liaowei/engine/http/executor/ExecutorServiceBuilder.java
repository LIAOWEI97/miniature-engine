package com.liaowei.engine.http.executor;

import java.util.concurrent.*;

/**
 * 线程池
 *
 * @author liaowei
 * @date 2020/06/12
 */
public class ExecutorServiceBuilder {
    private ThreadPoolExecutor threadPoolExecutor;
    private RejectedExecutionHandler  defaultHandler =
            new ThreadPoolExecutor.AbortPolicy();

    public ExecutorServiceBuilder() {
        this(100);
    }

    public ExecutorServiceBuilder(int maxQueueSize) {
        threadPoolExecutor = new ThreadPoolExecutor(1 ,
                1, 0,
                TimeUnit.NANOSECONDS,
                new LinkedBlockingQueue<>(maxQueueSize),
                Executors.defaultThreadFactory(), defaultHandler);
    }

    public ExecutorServiceBuilder setCorePoolSize(int corePoolSize) {
        if (corePoolSize > 0) {
            threadPoolExecutor.setCorePoolSize(corePoolSize);
        }
       return this;
    }

    public ExecutorServiceBuilder setMaximumPoolSize(int maximumPoolSize) {
        if (maximumPoolSize > 0 &&
                maximumPoolSize >= threadPoolExecutor.getCorePoolSize()) {
            threadPoolExecutor.setMaximumPoolSize(maximumPoolSize);
        }
        return this;
    }

    public ExecutorServiceBuilder setKeepAliveTime(long keepAliveTime, TimeUnit unit) {
        if (keepAliveTime >= 0 && unit != null) {
            threadPoolExecutor.setKeepAliveTime(keepAliveTime, unit);
        }
        return this;
    }

    public ExecutorServiceBuilder setThreadFactory(ThreadFactory threadFactory) {
        if (threadFactory != null) {
            threadPoolExecutor.setThreadFactory(threadFactory);
        }
        return this;
    }

    public ExecutorServiceBuilder setRejectedHandler(RejectedExecutionHandler rejectedHandler) {
        if (rejectedHandler != null) {
            threadPoolExecutor.setRejectedExecutionHandler(rejectedHandler);
        }
        return this;
    }

    public ExecutorService build() {
        return threadPoolExecutor;
    }
}
